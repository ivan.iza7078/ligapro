from django.urls import path
from . import views


urlpatterns = [
    path('', views.listadoEquipo),
    path('guardarEquipo/', views.guardarEquipo),
    path('eliminarEquipo/<int:id_equi>/', views.eliminarEquipo),
    path('editarEquipo/<int:id_equi>/', views.editarEquipo),
    path('procesarActualizacionEquipo/', views.procesarActualizacionEquipo),

    #Posicion
    path('listadoPosicion/', views.listadoPosicion),
    path('guardarPosicion/', views.guardarPosicion),
    path('eliminarPosicion/<int:id_pos>/', views.eliminarPosicion),
    path('editarPosicion/<int:id_pos>/', views.editarPosicion),
    path('procesarActualizacionPosicion/', views.procesarActualizacionPosicion),



]
