# Generated by Django 5.0.6 on 2024-06-26 17:43

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id_equi', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_equi', models.CharField(max_length=500)),
                ('siglas_equi', models.CharField(max_length=25)),
                ('fundacion_equi', models.IntegerField()),
                ('region_equi', models.CharField(max_length=25)),
                ('numero_titulos_equi', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Posicion',
            fields=[
                ('id_pos', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_pos', models.CharField(max_length=150)),
                ('descripcion_pos', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Jugador',
            fields=[
                ('id_jug', models.AutoField(primary_key=True, serialize=False)),
                ('apellido_jug', models.CharField(max_length=500)),
                ('nombre_jug', models.CharField(max_length=500)),
                ('estatura_jug', models.FloatField()),
                ('salario_jug', models.DecimalField(decimal_places=2, max_digits=10)),
                ('estado_jug', models.CharField(max_length=25)),
                ('fk_id_equi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='campeonato.equipo')),
                ('fk_id_pos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='campeonato.posicion')),
            ],
        ),
    ]
