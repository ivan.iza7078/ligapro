from django.contrib import admin
from .models import Equipo, Jugador, Posicion
# Register your models here.
admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Posicion)