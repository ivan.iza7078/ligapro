from django.shortcuts import render, redirect
from .models import Posicion, Equipo, Jugador
from django.db.models.deletion import ProtectedError
from django.contrib import messages
# Create your views here.
def listadoEquipo(request):
    EquipoBdd = Equipo.objects.all()
    return render(request, 'listadoEquipo.html', {'equipos': EquipoBdd})

def guardarEquipo(request):
    if request.method == 'POST':
        nombre_equi = request.POST.get("nombre_equi")
        siglas_equi = request.POST.get("siglas_equi")
        fundacion_equi = request.POST.get("fundacion_equi")
        region_equi = request.POST.get("region_equi")
        numero_titulos_equi = request.POST.get("numero_titulos_equi")
        equipo = Equipo.objects.create(
            nombre_equi=nombre_equi,
            siglas_equi=siglas_equi,
            fundacion_equi=fundacion_equi,
            region_equi=region_equi,
            numero_titulos_equi=numero_titulos_equi
            
        )

        messages.success(request, 'CLIENTE GUARDADO EXITOSAMENTE')
        return redirect('/')

def eliminarEquipo(request, id_equi):
    try:
        cliente_eliminar = Equipo.objects.get(id_equi=id_equi)
        try:
            cliente_eliminar.delete()
            messages.success(request, 'CLIENTE ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            messages.warning(request, 'No se puede eliminar el cliente porque existen objetos relacionados.')
    except Equipo.DoesNotExist:
        messages.error(request, 'El cliente no existe')

    return redirect('/')

def editarEquipo(request, id_equi):
    try:
        cliente_editar = Equipo.objects.get(id_equi=id_equi)
        clientes_bdd = Equipo.objects.all()
        return render(request, 'editarEquipo.html', {'equipo': cliente_editar, 'equipos': clientes_bdd})
    except Equipo.DoesNotExist:
        messages.error(request, 'El cliente no existe')
        return redirect('/')
    
def procesarActualizacionEquipo(request):
        if request.method == 'POST':
            id_equi = request.POST.get("id_equi")
            nombre_equi = request.POST.get("nombre_equi")
            siglas_equi = request.POST.get("siglas_equi")
            fundacion_equi = request.POST.get("fundacion_equi")
            region_equi = request.POST.get("region_equi")
            numero_titulos_equi = request.POST.get("numero_titulos_equi")

        try:
            equipoEditar = Equipo.objects.get(id_equi=id_equi)

            equipoEditar.nombre_equi = nombre_equi
            equipoEditar.siglas_equi = siglas_equi
            equipoEditar.fundacion_equi = fundacion_equi
            equipoEditar.region_equi = region_equi
            equipoEditar.numero_titulos_equi = numero_titulos_equi
            equipoEditar.save()

            messages.success(request, 'EQUIPO ACTUALIZADO EXITOSAMENTE')
        except Equipo.DoesNotExist:
            messages.error(request, 'El equipo no existe')

        return redirect('/')

#Posicion
#listadoPosicion
def listadoPosicion(request):
    PosicionBdd = Posicion.objects.all()
    return render(request, 'listadoPosicion.html', {'posiciones': PosicionBdd})

#guardarPosicion
def guardarPosicion(request):
    if request.method == 'POST':
        nombre_pos = request.POST.get("nombre_pos")
        descripcion_pos = request.POST.get("descripcion_pos")
        posicion = Posicion.objects.create(
            nombre_pos=nombre_pos,
            descripcion_pos=descripcion_pos,    
        )
        messages.success(request, 'POSICION GUARDADO EXITOSAMENTE')
        return redirect('/listadoPosicion')
#eliminarPosicion
def eliminarPosicion(request, id_pos):
    try:
        posicion_eliminar = Posicion.objects.get(id_pos=id_pos)
        try:
            posicion_eliminar.delete()
            messages.success(request, 'POSICION ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            messages.warning(request, 'No se puede eliminar el cliente porque existen objetos relacionados.')
    except Posicion.DoesNotExist:
        messages.error(request, 'El cliente no existe')

    return redirect('/listadoPosicion')

#editarPosicion
def editarPosicion(request, id_pos):
    try:
        posicion_editar = Posicion.objects.get(id_pos=id_pos)
        posicionbdd = Posicion.objects.all()
        return render(request, 'editarPosicion.html', {'posicion': posicion_editar, 'posiciones': posicionbdd})
    except Equipo.DoesNotExist:
        messages.error(request, 'La posicion no existe')
        return redirect('/listadoPosicion')

#procesarActualizarPosicion
def procesarActualizacionPosicion(request):
        if request.method == 'POST':
            id_pos = request.POST.get("id_pos")
            nombre_pos = request.POST.get("nombre_pos")
            descripcion_pos = request.POST.get("descripcion_pos")
            

        try:
            posicionEditar = Posicion.objects.get(id_pos=id_pos)

            posicionEditar.nombre_pos = nombre_pos
            posicionEditar.descripcion_pos = descripcion_pos
            posicionEditar.save()

            messages.success(request, 'POSICION ACTUALIZADO EXITOSAMENTE')
        except Posicion.DoesNotExist:
            messages.error(request, 'El equipo no existe')

        return redirect('/listadoPosicion')

    